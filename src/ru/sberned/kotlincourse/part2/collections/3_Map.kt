package ru.sberned.kotlincourse.part2.collections

/** Список пар ключ-значение
 * Связь между ключем и значением происходит через функцию to */

fun main() {
    val map = mapOf(1 to "one", 3 to "three", 9 to "nine")      // immutable

    println(map.getValue(1))
    //или
    println(map[1])

    println(map.getValue(100))    // !!! выбросит исключение NoSuchElementException при несуществующем ключе
    var valueBy100 = map.getOrElse(100) { "Not found value with key 100" }    // анонимная функция
    println(valueBy100)
    // или
    valueBy100 = map.getOrDefault(100, "No such number")                // или значение по умочанию
    println(valueBy100)

    // перебор коллекции
    for ((k, v) in map) {
        println("There are $v $k")
    }
    // или
    map.forEach { (k, v) -> println("There are $v $k") }

    val mutableMap = mutableMapOf("One" to 1, "Two" to 2, "Three" to 3)
    val hashMap = hashMapOf(1 to "Иванов", 2 to "Петров", 3 to "Сидоров")
    val sortedMap = sortedMapOf(2 to "Второй", 1 to "Первый", 3 to "Третьий")
}