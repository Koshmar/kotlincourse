package ru.sberned.kotlincourse.part2.collections

/** Set представляет неупорядоченный набор объектов, который хранит только уникальные объекты.  */
fun main() {
    val intSet = setOf(1, 2, 3)     // неизменяемое множество java.util.LinkedHashSet

    val intsHashSet = hashSetOf(1, 2, 3, 4)
    val intSortedSet = sortedSetOf(9, 1, 8, 3)
    val intLinkedHashSet = linkedSetOf(15, 6, 7, 2, 3)
    val intMutableSet = mutableSetOf(3, 27, 66, 12, 10)

    intSet.elementAt(1)             // compile error - intSet[1]
}