package ru.sberned.kotlincourse.part2.collections

/** Kotlin не имеет собственной библиотеки коллекций и использует классы коллекций Java.
 * Kotlin расширяет дополнительными возможностями коллекции Java
 * Коллекции разделяются на изменяемые (mutable) и неизменяемые (immutable)
 * Mutable - может изменяться, в нее можно добавлять, в ней можно изменять, удалять элементы.
 * Immutable - также поддерживает добавление, замену и удаление данных, однако в процессе коллекция будет заново пересоздаваться. */
fun main() {
    val immutableNums =
        listOf(1, 2, 3, 4, 5, null)   // По факту объект интерфейса List будет представлять класс в Java - java.util.Arrays$ArrayList

    val numbers: ArrayList<Int> = arrayListOf(1, 2, 3, 4, 5)
    //var numbers2: MutableList<Int> = mutableListOf(5, 6, 7)   // или так

    //несколько полезных расширений для списков и множеств
    numbers.first() == 1
    numbers.last() == 4
    numbers.max()
    numbers.min()
    numbers.sum()
    numbers.filter { it % 2 == 0 }      // возвратит [2, 4]
    immutableNums.filterNotNull()       // возвратит [1, 2, 3, 4, 5]
    numbers.any { it >= 5 }             // true - есть ли элемент больше или равен 5
    numbers.take(2)                  // [1, 2] - первые n элементов
    numbers.reversed()                  // элементы в обратном порядке
    numbers.sort()                      // отсортировать
    numbers.shuffle()                   // перемешать элементы
    numbers.joinToString(",")   // для вывода в строку
}