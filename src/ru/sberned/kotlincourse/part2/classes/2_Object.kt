package ru.sberned.kotlincourse.part2.classes

/** Ключевое слово object одновременно объявляет класс и создаёт его экземпляр
 * С его помощью можно реализовать шаблон Singleton
 * Декларированный объект инициализируется лениво, в момент первого к нему доступа */
object Counter {
    private var count: Int = 0
    fun currentCount() = count
    fun increment() = ++count
}

fun main() {
    Counter.increment()
    println(Counter.currentCount())
}