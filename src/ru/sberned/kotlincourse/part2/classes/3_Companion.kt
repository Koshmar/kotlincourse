package ru.sberned.kotlincourse.part2.classes

/** в Котлине нет static классов/методов */

/** можно объявить функцию вне класса */
fun globalFunction(a: Int, b: Int) = a + b

/** можно использовать companion object */
class ToBeCalled {
    companion object {
        fun callMe() = println("You are calling me :)")
    }
}
fun main() {
    ToBeCalled.callMe()
}