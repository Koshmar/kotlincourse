package ru.sberned.kotlincourse.part2.exceptions

import java.io.BufferedReader
import java.io.File

/** Все исключения в Kotlin являются наследниками класса Throwable.
 * У каждого исключения есть сообщение, трассировка стека и причина, по которой это исключение вероятно было вызвано.*/

/** Try - это выражение */
fun readNumber(reader: BufferedReader): Int? =
    try {
        val line = reader.readLine()
        Integer.parseInt(line)              // результат функции
    } catch (e: NumberFormatException) {
        null                                // результат в случае ошибки
    } finally {
        reader.close()
    }

/** В языке Kotlin нет проверяемых исключений */

fun main(){
    val file = File("text.txt")
    println(readNumber(file.bufferedReader()))

    val person = Person(null)
    // допустимо использовать выражение throw в качестве части элвис-выражения
    var personName = person.name ?: throw IllegalArgumentException("Name required")
    // или
    //var personName = person.name ?: fail("Name required")
}

/** Типом выражения throw является специальный тип под названием Nothing */
fun fail(message: String) :Nothing = throw IllegalArgumentException(message)


data class Person(val name:String?)



