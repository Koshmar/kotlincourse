package ru.sberned.kotlincourse.part2.function

/** Функция apply работает почти так же, как with, но возвращает объект, переданный в аргументе. */

fun printAlphabetWithApply() = StringBuilder().apply{
    for (letter in 'A'..'Z'){
        append(letter)
    }
}.toString()

/** Как правило apply применяется для конфигурирования объекта */
data class Person(val name:String, var age: Int)

fun main() {
    println(printAlphabetWithApply())

    val person = Person("Name", 20)
    person.apply {
        //name = "New name"     // compile error
        age = 21
    }
    println(person)
}
