package ru.sberned.kotlincourse.part2.function

/** Анонимные функции */

fun main() {
    val sumV1: (Int, Int) -> Int = fun(x: Int, y: Int) = x + y
    println(sumV1(10, 15))

    val messageFunction = fun(lang: String) =
        "Добро пожаловать на курс по языку $lang!"
    println(messageFunction("Kotlin"))
}