package ru.sberned.kotlincourse.part2.function

/** что выбрать анонимную функцию или lambda выражение? */

fun processElements1(list: List<Int>): Boolean {
    list.forEach { element ->
        if (!needToProcessElement(element)) return false   // возврат из функции processElements1()
    }
    return true
}

fun processElements2(list: List<Int>) {
    list.forEach { element ->
        if (!needToProcessElement(element)) return@forEach  // возврат из блока
        process(element)
    }
}


fun processElements3(list: List<Int>) {
    list.forEach(fun(element) {
        if (!needToProcessElement(element)) return          // возврат из блока
        process(element)
    })
}



fun needToProcessElement(element: Int): Boolean = element % 2 == 0
fun process(element: Int) = println(element)