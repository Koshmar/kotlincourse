package ru.sberned.kotlincourse.part2.function

/** Функция let позволяет выполнить над объектом манипуляции и вернуть результат этих дейстывий */

fun main() {
    val numbers = listOf("one", "two", "three", "four")
    val modifiedFirstItem = numbers.first().let { firstItem ->
        println("The first item of the list is '$firstItem'")
        if (firstItem.length >= 5) firstItem else "!$firstItem!"
    }.toUpperCase()
    println("First item after modifications: '$modifiedFirstItem'")
}