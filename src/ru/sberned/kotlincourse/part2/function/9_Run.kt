package ru.sberned.kotlincourse.part2.function

import java.util.*

/** Функция run используется для списка манипуляций над объектом */

fun main() {
    // 1. цепочка трансформаций
    val date: Int = Calendar.getInstance().run {
        set(Calendar.YEAR, 2030)
        get(Calendar.DAY_OF_YEAR) //return value of run
    }
    println(date)

    // 2. замена if (object != null)
    val text: String? = null
    val len = text?.run {
        println("get length of $this")
        length //this can be omitted
    } ?: 0
    println(len)
}