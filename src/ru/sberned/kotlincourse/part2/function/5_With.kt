package ru.sberned.kotlincourse.part2.function

/** Функция with позволяет выполнить несколько операций над одним объектом, не повторяя его имени.
 * Функция принимает два аргумента - объект и лямбда-выражение.
 * Первый аргумент преобразуется в получатель лямбда-выражения.
 * К получателю можно обращаться через this
 * Функция возвращает результат последнего выражения в теле лямбда-выражения */

fun printAlphabet() = with(StringBuilder()) {
    for (letter in 'A'..'Z') {
        append(letter)
    }
    toString()
}

fun main() = println(printAlphabet())