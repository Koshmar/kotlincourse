package ru.sberned.kotlincourse.part2.function

/** Лямбда-выражение или необъявленная функция, которая немедленно используется в качестве выражения
 * { arguments -> function body } */

fun main() {
    val lambda = { println("Hello Lambda!") }
    lambda.invoke()             // вызов функции
    lambda()                    // вызов функции

    val sum = { a: Int, b: Int -> a * b } // (Int, Int)->Int

    /** Есть перегруженная версия функции count (и множество других) с возможностью передавать функции */
    val charNumber = "азбука".count { letter -> letter == 'а' }
    println(charNumber)         // выводит 2
}