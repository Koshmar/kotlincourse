package ru.sberned.kotlincourse.part2.function

/** Функция also позволяет выполнить действия над объектом it
 * Применима для выполнения дополнительных действий над объектом, например, печать или логгирование */

fun main() {
    val numbers = mutableListOf("one", "two", "three")
    numbers
        .also { println("The list elements before adding new one: $it") }
        .add("four")
}