package ru.sberned.kotlincourse.part2.function

/** Kotlin поддерживает функции-расширения и свойства-расширения */
fun <T> MutableList<T>.swap(index1: Int, index2: Int) {
    val tmp = this[index1] // 'this' даёт ссылку на список
    this[index1] = this[index2]
    this[index2] = tmp
}

fun main() {
    val mutableList = mutableListOf(1, 2, 3)
    mutableList.swap(0, 2)
}