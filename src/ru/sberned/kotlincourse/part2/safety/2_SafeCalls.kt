package ru.sberned.kotlincourse.part2.safety

/** Безопасные вызовы */

data class Person(val country: Country?)

data class Country(val code: String?)

fun main() {

    val person: Person? = Person(Country("RU"))
    val res = person?.country?.code
    println(res)

/*val person: Person? = Person(Country(null))
val res = person?.country?.code
println(res)*/

/*val person: Person? = Person(null)
val res = person?.country?.code
println(res)*/

}