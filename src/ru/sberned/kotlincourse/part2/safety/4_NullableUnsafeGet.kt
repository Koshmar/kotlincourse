package ru.sberned.kotlincourse.part2.safety

/** Nullable Unsafe Get */

fun main() {
    val nullString: String? = "value"   //if null - kotlin.KotlinNullPointerException
    println(nullString!!.length)
}