package ru.sberned.kotlincourse.part2.safety

/** Примеры типов Nullable и Non-Nullable */
fun main() {
    var notNullToday: String = "Not null"
    //notNullToday = null       // compiler error

    var canBeNull: String? = "Null"
    canBeNull = null            // ok

    //notNullToday = canBeNull  // compiler error
}