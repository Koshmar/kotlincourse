package ru.sberned.kotlincourse.part2.safety

/** Elvis Operator */

fun main() {
    val value: String? = null
    val valueLength = value?.length ?: -1
    println(valueLength)
}