package ru.sberned.kotlincourse.part1.expression

fun cycleForExample(collection: List<String>) {
    for (item in collection)
        println(item)

    for ((index, value) in collection.withIndex())
        println("$value with index $index")

}

fun whileExample() {
    var counter = 3
    while (counter > 0) {
        println("Обратный отсчет ...${counter--}")
    }
}

fun main() {
    cycleForExample(listOf("kotlin", "java", "scala"))
    whileExample()
}