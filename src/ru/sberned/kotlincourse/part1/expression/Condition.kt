package ru.sberned.kotlincourse.part1.expression

fun ifConditionExample(firstNum: Int, secondNum: Int) {
    // обычное использование
    var maxSimple = firstNum
    if (firstNum < secondNum)
        maxSimple = secondNum

    // с блоком else
    var max: Int
    if (firstNum > secondNum) {
        max = firstNum
    } else {
        max = secondNum
    }

    //Отсутствует тернарный оператор!
    // в виде выражения
    val maxExpression = if (firstNum > secondNum) firstNum else secondNum
}

fun whenConditionExample(inputNum: Int) {
    when (inputNum) {
        1 -> println("x == 1")
        2 -> println("x == 2")
        else -> println("x is neither 1 nor 2")
    }

    val validNumbers = arrayOf(0, 100, 1000)
    when (inputNum) {
        in 1..10 -> println("x is in the range")
        in validNumbers -> println("x is valid")
        !in 10..20 -> println("x is outside the range")
        else -> println("none of the above")
    }

    val whenResult = when (inputNum) {
        1 -> inputNum * 100
        else -> inputNum * 1000
    }
}

fun main() {
    ifConditionExample(firstNum = 1, secondNum = 2)
    whenConditionExample(inputNum = 10)
}