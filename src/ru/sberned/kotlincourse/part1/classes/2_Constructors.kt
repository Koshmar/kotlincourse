package ru.sberned.kotlincourse.part1.classes

class SecondClass(val name: String) {            //первичный конструктор
    constructor(names: List<String>) : this(     //вторичный конструктор
        name = names[0]
    )
    val smallName = this.name.toLowerCase()

    init {
        println("init block")   //тут будет инициализирующий код
    }
}

fun main() {
    val firstClass = SecondClass(arrayListOf("Hello"))  //создание экземпляра класса
    println(firstClass.name)
    println(firstClass.smallName)
}