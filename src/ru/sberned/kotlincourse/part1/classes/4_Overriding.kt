package ru.sberned.kotlincourse.part1.classes

/** Переопределение методов и свойств */
open class SimplePerson(val name: String) {
    open val fullInfo: String
        get() = "Name $name"

    open fun display() {         // open - чтобы можно было переопределить
        println("Name: $name")
    }
}

open class Employee(val company: String, name: String) : SimplePerson(name) {
    override val fullInfo: String
        get() = "Name: $name Company: $company"

    final override fun display() {    // override - используем при переопределении
        println("Name: $name Company: $company")
    }
}

class Manager(company: String, name: String) : Employee(company, name) {
    // compile error - final у предка не дает переопределить функцию
    /*
    final override fun display() {
    }
    */
}

fun main() {
    val person = SimplePerson("Предок")
    val employee = Employee("ЦНС", "Потомок")
}