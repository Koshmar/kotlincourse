package ru.sberned.kotlincourse.part1.classes

/** Классы. Наследование */
class Example   // Неявно наследуется от Any
// Any содержит только equals(), hashCode(), и toString()

/** наследование класса с конструктором */
open class Person(val name: String)             // open - чтобы позволить отнаследоваться от класса Person
class SystemUser(val company: String, name: String): SimplePerson(name)     // наследний обязан в конструкторе вызвать конструктор предка

/** в случае отсутствия у наследника явного конструктора */
class Customer: SimplePerson{
    var registrationAddress: String="undefined"
    constructor(name: String, address:String) : super(name){
        registrationAddress = address
    }
}