package ru.sberned.kotlincourse.part1.classes

/** Enums или перечисления - набор логически связанных констан */
enum class Day {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}

enum class Color(val rgb: Int) {
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF)
}

fun main(args: Array<String>) {
    val dayFriday: Day = Day.FRIDAY
    println(dayFriday.name)        // название константы
    println(dayFriday.ordinal)     // порядок

    println(Day.values().joinToString(","))
    println(Day.valueOf("SUNDAY"))
}