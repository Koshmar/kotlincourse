package ru.sberned.kotlincourse.part1.classes

/** Интерфейс
 * Интерфейсы представляют контракт, который должен реализовать класс. */

interface Movable{
    fun move()      // определение функции без реализации
    fun stop(){     // определение функции с реализацией по умолчанию
        println("Остановка")
    }
}

/** реализации */
class Car : Movable{
    override fun move(){
        println("Машина едет")
    }
}

class Aircraft : Movable{
    override fun move(){
        println("Самолет летит")
    }
    override fun stop(){
        println("Приземление")
    }
}

fun main() {
    val m1: Movable = Car()
    val m2: Movable = Aircraft()
    // val m3: Movable = Movable() напрямую объект интерфейса создать нельзя

    m1.move()
    m1.stop()

    m2.move()
    m2.stop()
}