package ru.sberned.kotlincourse.part1.classes

/** самый простой пример класса */
class EmptyClass

/** класс с первичным конструктором */
class FirstClass(val name: String)

/** класс с первичным конструктором с дефолтным значением */
class ClassWithDefault(val customerName: String = "default")
