package ru.sberned.kotlincourse.part1.classes

/** Абстрактные классы и методы */

// абстрактный класс фигуры
abstract class Figure {
    // абстрактный метод для получения периметра
    abstract fun perimeter(): Float

    // абстрактный метод для получения площади
    abstract fun area(): Float

    fun printFigureInfo() {
        println("perimeter: ${perimeter()} area: ${area()}")
    }
}

// производный класс прямоугольника
class Rectangle(val width: Float, val height: Float) : Figure() {
    // переопределение получения периметра
    override fun perimeter(): Float {
        return width * 2 + height * 2
    }

    // переопрелеление получения площади
    override fun area(): Float {
        return width * height
    }
}

fun main() {
    //val figure: Figure = Figure()     //compile error
    val rectangle: Figure = Rectangle(100f, 100f)
    rectangle.printFigureInfo()
}