package ru.sberned.kotlincourse.part1.types

fun main() {
    var mutableVariable: String = "Hello"
    mutableVariable = "Hi"

    val immutableValue: String = "Hello"
    //immutableValue = "Hi"     //compile error
}
