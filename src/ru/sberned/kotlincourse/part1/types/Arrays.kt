package ru.sberned.kotlincourse.part1.types

fun main() {

    val simpleArray = arrayOf(1, 2, 3)      // intArrayOf(1, 2, 3) без затрат на обертки
    simpleArray[0] = 100// == arrayWithElements.set(0, 100)
    println(simpleArray.joinToString(" "))

    val simpleArrayWithConstructor = Array(3) { i -> i*5}
    println(simpleArrayWithConstructor.joinToString(" "))

}