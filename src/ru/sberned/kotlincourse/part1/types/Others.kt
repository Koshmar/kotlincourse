package ru.sberned.kotlincourse.part1.types

fun main() {
    val simpleChar = '1'
    println(simpleChar.toInt())    //только явные преобразования

    val simpleBoolean = true
    println(!simpleBoolean)

    val simpleString = "Hello World"
    val longString = """
        Ах, обмануть меня не трудно!..
        Я сам обманываться рад!
    """
    println(simpleString)
    println(longString.trimIndent())
}