package ru.sberned.kotlincourse.part1.types

fun main() {
    val simpleInt = 123         //Int
    val simpleLong = 123L       //Long
    val simpleDouble = 123.5    //Double
    val simpleFloat = 123.5f    //Float

    val simpleCardNumber = 1234_5678_9012_3456L     //нижние подчеркивания для удобства чтения числовых констант

    var nullValue: Int? = null      //если есть вероятность того, что объект будет null

    //val intToLong: Long = simpleInt //compile error
    val intToLong: Long = simpleInt.toLong()    //отсутствуют неявные преобразования типов
    val intLongSum = simpleInt + simpleLong     //для арифметики ОК
}