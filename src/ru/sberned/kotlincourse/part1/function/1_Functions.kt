package ru.sberned.kotlincourse.part1.function

/** функции объявляются с помощью ключевого слова fun */
fun firstFun() = println("Just for fun")    //функция ничего не возвращает

fun addFive(num: Int): Int {
    return num + 5
}

//fun addFive(num: Int) = num + 5   // так короче

fun addOne(num: Int, one:Int = 1) = num + one       // с аргументом по умолчанию

// Run!
fun main() {
    firstFun()
    println(addFive(5))
    println(addOne(5))
    println(addOne(num = 5, one = 10))      //Имена параметров могут быть явно указаны при вызове функций
}